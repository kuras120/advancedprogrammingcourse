package pk.advancedprogrammingcourse.task3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pk.advancedprogrammingcourse.task3.model.Book;

@Repository("bookRepository")
public interface BookRepository extends JpaRepository<Book, Long> {

    Book findByTitle(String title);
}
