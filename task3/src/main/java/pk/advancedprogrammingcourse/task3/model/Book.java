package pk.advancedprogrammingcourse.task3.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Random;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Book {

    @Column(nullable = false)
    @NotEmpty(message = "Title cannot be empty")
    private String title;

    @Column(nullable = false)
    @NotEmpty(message = "Author cannot be empty")
    private String author;

    @Column(nullable = false)
    @NotEmpty(message = "Year cannot be empty")
    private String year;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    public Book(int min, int max, long id) {
        Random random = new Random();
        this.title = StringUtils.capitalize(randomStringGenerator(random, random.nextInt(10) + 5));
        this.author =
                StringUtils.capitalize(randomStringGenerator(random, random.nextInt(6) + 6))
                .concat(" ")
                .concat(StringUtils.capitalize(randomStringGenerator(random, random.nextInt(6) + 6)));
        this.year = Integer.toString(random.nextInt((max - min) + 1) + min);
        this.id = id;
    }

    private String randomStringGenerator(Random random, int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        return random.ints(leftLimit, rightLimit + 1)
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}

