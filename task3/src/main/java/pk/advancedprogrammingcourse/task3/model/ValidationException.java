package pk.advancedprogrammingcourse.task3.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@AllArgsConstructor
public class ValidationException extends Exception {

    @Getter
    private final Map<String, String> errors;
}
