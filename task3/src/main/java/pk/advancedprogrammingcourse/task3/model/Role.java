package pk.advancedprogrammingcourse.task3.model;

public enum Role {
    ADMIN,
    USER
}
