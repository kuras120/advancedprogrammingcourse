package pk.advancedprogrammingcourse.task3.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {

    private String message;
    private String methodName;
    private String className;
    private String fileName;
    private int lineNumber;
    private Map<String, String> errors;
    private Map<String, String[]> parameters;
    private Object body;
}
