package pk.advancedprogrammingcourse.task3.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pk.advancedprogrammingcourse.task3.model.ErrorResponse;
import pk.advancedprogrammingcourse.task3.model.ValidationException;

import java.util.Collections;

@RestControllerAdvice
@RequiredArgsConstructor
public class ExceptionRestController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse(
                ex.getMessage(),
                ex.getStackTrace()[0].getMethodName(),
                ex.getStackTrace()[0].getClassName(),
                ex.getStackTrace()[0].getFileName(),
                ex.getStackTrace()[0].getLineNumber(),
                Collections.emptyMap(),
                Collections.emptyMap(),
                null
        );
        if (ex instanceof ValidationException) errorResponse.setErrors(((ValidationException) ex).getErrors());
        return ResponseEntity
                .status(500)
                .body(errorResponse);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(
            Exception ex,
            Object body,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request
    ) {
        ErrorResponse errorResponse = new ErrorResponse(
                ex.getMessage(),
                ex.getStackTrace()[0].getMethodName(),
                ex.getStackTrace()[0].getClassName(),
                ex.getStackTrace()[0].getFileName(),
                ex.getStackTrace()[0].getLineNumber(),
                Collections.emptyMap(),
                request.getParameterMap(),
                body
        );
        if (ex instanceof ValidationException) errorResponse.setErrors(((ValidationException) ex).getErrors());
        return ResponseEntity
                .status(status)
                .body(errorResponse);
    }
}
