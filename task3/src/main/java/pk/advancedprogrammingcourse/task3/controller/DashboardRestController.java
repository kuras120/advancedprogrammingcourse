package pk.advancedprogrammingcourse.task3.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import pk.advancedprogrammingcourse.task3.model.Book;
import pk.advancedprogrammingcourse.task3.model.User;
import pk.advancedprogrammingcourse.task3.model.ValidationException;
import pk.advancedprogrammingcourse.task3.service.IDashboardService;
import pk.advancedprogrammingcourse.task3.service.IUserService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class DashboardRestController {

    private final IDashboardService dashboardService;

    private final IUserService userService;

    @GetMapping("/user")
    public ResponseEntity<?> getUser() {
        User user = userService.getSessionUser();
        if (user == null)
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        else
            return ResponseEntity.ok(user);
    }

    @GetMapping("/clear")
    public ResponseEntity<?> clearSession(HttpSession session) {
        session.invalidate();
        return ResponseEntity.ok().build();
    }

    @GetMapping("/dashboard")
    public ResponseEntity<List<Book>> getBooks() {
        return ResponseEntity.ok(dashboardService.getBooks());
    }

    @PostMapping("/dashboard")
    public ResponseEntity<Book> addBook(
            @Valid @RequestBody Book book,
            BindingResult bindingResult
    ) throws ValidationException {
        if (bindingResult.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (var obj : bindingResult.getAllErrors()) {
                if (obj instanceof FieldError) {
                    errors.put(((FieldError) obj).getField(), obj.getDefaultMessage());
                }
            }
            throw new ValidationException(errors);
        }
        return ResponseEntity.status(201).body(dashboardService.addBook(book));
    }

    @DeleteMapping("/dashboard")
    public ResponseEntity<Book> deleteBook(
            @RequestParam(value = "bookId") long bookId
    ) throws ValidationException {
        Book book = dashboardService.removeBook(bookId);
        if (book == null) throw new ValidationException(Map.of("bookId", "Book does not exist"));
        return ResponseEntity.ok(book);
    }
}
