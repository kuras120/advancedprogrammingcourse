package pk.advancedprogrammingcourse.task3.service;

import pk.advancedprogrammingcourse.task3.model.Book;

import java.util.List;

public interface IDashboardService {

    List<Book> getBooks();

    Book addBook(Book book);

    Book removeBook(Book book);

    Book removeBook(long id);
}
