package pk.advancedprogrammingcourse.task3.service;

import org.springframework.security.core.GrantedAuthority;
import pk.advancedprogrammingcourse.task3.model.User;

import java.util.Collection;

public interface IUserService {

    User getSessionUser();

    Collection<? extends GrantedAuthority> getSessionAuthorities();
}
