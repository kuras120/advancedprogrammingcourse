package pk.advancedprogrammingcourse.task3.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pk.advancedprogrammingcourse.task3.model.User;
import pk.advancedprogrammingcourse.task3.repository.UserRepository;

import java.util.Collection;

@Service("userService")
@RequiredArgsConstructor
public class UserService implements UserDetailsService, IUserService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByUsername(s);
    }

    @Override
    public User getSessionUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails)
            return (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        else return null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getSessionAuthorities() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities();
    }
}
