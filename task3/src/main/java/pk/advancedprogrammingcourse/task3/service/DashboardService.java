package pk.advancedprogrammingcourse.task3.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pk.advancedprogrammingcourse.task3.model.Book;
import pk.advancedprogrammingcourse.task3.repository.BookRepository;

import java.util.List;

@Service("dashboardService")
@RequiredArgsConstructor
public class DashboardService implements IDashboardService {

    private final BookRepository bookRepository;

    @Override
    public List<Book> getBooks() {
        return bookRepository.findAll();
    }

    @Override
    public Book addBook(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public Book removeBook(Book book) {
        bookRepository.delete(book);
        return book;
    }

    @Override
    public Book removeBook(long id) {
        Book book = bookRepository.findById(id).orElse(null);
        if (book != null) {
            bookRepository.deleteById(id);
            return book;
        }
        return null;
    }
}
