INSERT INTO USER
    (USERNAME, PASSWORD, ROLE)
VALUES
    ('user1', '$2y$12$blP6X8KZp6qqfBXCFFtUcuhF9FcIsoqfe3Hy7VhkWN08RGsN2Qz26', 'USER'),
    ('user2', '$2y$12$rb/bv1F94wMeI9Y4FZi8GecRRPbeAA8Mu1CqMVv25mUwG9FUtkcw6', 'USER'),
    ('admin', '$2y$12$uD//zuZvBjwGC8ratWpI6OWehyvRUOjh.SblVAsRIXY31/HsMR5kO', 'ADMIN');

INSERT INTO BOOK
    (AUTHOR, TITLE, YEAR)
VALUES ('Axl Boyce', 'The Dance of the Sphinx', 1988),
       ('Jeremy Arroyo', 'Solar Grieving', 2002),
       ('Noel Kaur', 'The Spell in the Night', 2015),
       ('Leona Cairns', 'Black Silk', 2019),
       ('Kairon Giles', 'The Eye of the Damned', 1939),
       ('Kaylan Deleon', 'Scoundrel''s Daughter', 1966),
       ('Bhavik Malone', 'The Hollow Bow', 1945),
       ('Kimberley Duncan', 'Cleric''s Gold', 998),
       ('Michael Drummond', 'The Saga of the Isle', 1999),
       ('Aaryan Harrison', 'The Prophecy in the Chasm', 2012);
