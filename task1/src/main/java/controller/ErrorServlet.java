package controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "controller.ErrorServlet", urlPatterns = {"/error"})
public class ErrorServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (statusCode != null) {
            request.setAttribute("status", statusCode);
            String message = "An unexpected error has occurred.";
            if (statusCode == 404 || statusCode == 405) {
                message = "The page you are looking for was not found.";
            }
            request.setAttribute("message", message);
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
