package controller;

import model.Book;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "controller.AdminServlet", urlPatterns = {"/admin"})
public class AdminServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String year = request.getParameter("year");
        List<Book> books = (List<Book>) request.getServletContext().getAttribute("books");
        if (title != null && author != null && year != null) {
            Book bookToDelete = books.stream().filter(book ->
                        book.getTitle().equals(title) &&
                        book.getAuthor().equals(author) &&
                        book.getYear().equals(year)).findFirst().orElse(null);
            books.remove(bookToDelete);
            request.getServletContext().setAttribute("books", books);
        }
        request.setAttribute("books", books);
        request.getRequestDispatcher("/admin.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        List<Book> books = (List<Book>) request.getServletContext().getAttribute("books");
        books.add(new Book(
                request.getParameter("title"),
                request.getParameter("author"),
                request.getParameter("year")
        ));
        request.getServletContext().setAttribute("books", books);
        response.sendRedirect(request.getContextPath() + "/admin");
    }
}
