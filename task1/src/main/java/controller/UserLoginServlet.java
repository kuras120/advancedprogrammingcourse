package controller;

import model.Role;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "controller.UserLoginServlet", urlPatterns = {"/login/user"})
public class UserLoginServlet extends HttpServlet {

    private Map<String, String> users;

    public void init() {
        users = new HashMap<String, String>() {{
            put("user1", "password1");
            put("user2", "password2");
            put("user3", "password3");
        }};
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        User user = new User(request.getParameter("login"), request.getParameter("password"), Role.USER);
        if (checkUser(user)) {
            request.getSession().setAttribute("user", user);
            response.sendRedirect(request.getContextPath() + "/dashboard");
        } else {
            response.sendRedirect(request.getContextPath() + "/loginFailed.jsp");
        }
    }

    private boolean checkUser(User user) {
        String password = this.users.get(user.getLogin());
        return password != null && password.equals(user.getPassword());
    }
}
