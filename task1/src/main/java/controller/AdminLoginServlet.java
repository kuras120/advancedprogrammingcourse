package controller;

import model.Role;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "controller.AdminLoginServlet", urlPatterns = {"/login/admin"})
public class AdminLoginServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String password = request.getParameter("password");
        if ("admin".equals(password)) {
            request.getSession().setAttribute("user", new User("admin", "admin", Role.ADMIN));
            response.sendRedirect(request.getContextPath() + "/admin");
        } else {
            response.sendRedirect(request.getContextPath() + "/loginFailed.jsp");
        }
    }
}
