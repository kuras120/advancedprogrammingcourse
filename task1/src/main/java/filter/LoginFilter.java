package filter;

import lombok.extern.slf4j.Slf4j;
import model.Role;
import model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/dashboard", "/admin"})
public class LoginFilter implements Filter {
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) {
            response.sendRedirect(request.getContextPath() + "/login");
        } else {
            if (!Role.ADMIN.equals(user.getRole()) && request.getRequestURI().contains("admin")) {
                response.sendRedirect(request.getContextPath() + "/dashboard");
            } else {
                chain.doFilter(req, res);
            }
        }
    }
}
