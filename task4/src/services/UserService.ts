import axios from 'axios';

export function login(username: string, password: string) {
    return axios.get('http://localhost:8080/user', {
        headers: {
            'Authorization': encodeBase64(username + ':' + password)
        },
        withCredentials: true
    });
}

export function logout() {
    return axios.get('http://localhost:8080/clear', {
        withCredentials: true
    });
}

export function getUser() {
    return axios.get('http://localhost:8080/user', {withCredentials: true});
}

export function encodeBase64(str: string) {
    return 'Basic ' + btoa(str);
}