import axios from "axios";

export function getBooks() {
    return axios.get('http://localhost:8080/dashboard', {withCredentials: true})
}

export function addBook(title: string, author: string, year: string) {
    return axios.post('http://localhost:8080/dashboard', {
        title: title,
        author: author,
        year: year
    }, {withCredentials: true})
}

export function deleteBook(id: number) {
    return axios.delete(`http://localhost:8080/dashboard?bookId=${id}`, {withCredentials: true})
}