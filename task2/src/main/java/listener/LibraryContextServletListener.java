package listener;

import model.Book;
import model.BookList;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.LinkedList;
import java.util.List;

@WebListener
public class LibraryContextServletListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent contextEvent) {
        int counter = 0;
        List<Book> books = new LinkedList<>();
        while (counter < 10) {
            books.add(new Book(1930, 2020, counter));
            counter++;
        }
        BookList bookList = new BookList(books, counter-1);
        contextEvent.getServletContext().setAttribute("books", bookList);
    }
}
