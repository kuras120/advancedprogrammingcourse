package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.Random;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {

    private String title;
    private String author;
    private String year;
    private long id;

    public Book(int min, int max, long id) {
        Random random = new Random();
        this.title = StringUtils.capitalize(randomStringGenerator(random, random.nextInt(10) + 5));
        this.author =
                StringUtils.capitalize(randomStringGenerator(random, random.nextInt(6) + 6))
                .concat(" ")
                .concat(StringUtils.capitalize(randomStringGenerator(random, random.nextInt(6) + 6)));
        this.year = Integer.toString(random.nextInt((max - min) + 1) + min);
        this.id = id;
    }

    private String randomStringGenerator(Random random, int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        return random.ints(leftLimit, rightLimit + 1)
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}

