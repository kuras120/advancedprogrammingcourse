package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.ExceptionResponse;
import model.Response;
import model.Role;
import model.User;
import org.apache.commons.io.IOUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "controller.LoginServlet", urlPatterns = {"/", "/login"})
public class LoginServlet extends HttpServlet {

    private Map<String, String> users;

    private Gson gson;

    @Override
    public void init() {
        GsonBuilder builder = new GsonBuilder();
        users = new HashMap<>() {{
            put("admin", "admin");
            put("user1", "password1");
            put("user2", "password2");
            put("user3", "password3");
        }};
        builder.setPrettyPrinting();
        gson = builder.create();
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        User user;
        response.setContentType("application/json;charset=UTF-8");
        try {
            user = gson.fromJson(IOUtils.toString(request.getReader()), User.class);
        } catch (Exception ex) {
            ExceptionResponse exResponse = new ExceptionResponse();
            exResponse.setData(Map.of("jsonParseException", ex.getLocalizedMessage()));
            exResponse.setStatus(500);
            response.setStatus(500);
            gson.toJson(exResponse, response.getWriter());
            return;
        }
        Map<String, String> messages = checkUser(user);
        if (messages.isEmpty()) {
            if ("admin".equals(user.getLogin())) user.setRole(Role.ADMIN);
            else user.setRole(Role.USER);
            request.getSession().setAttribute("user", user);
            response.addCookie(createToken(user));
            response.setStatus(200);
        } else {
            response.setStatus(400);
            gson.toJson(new Response(messages, 400), response.getWriter());
        }
    }

    private Cookie createToken(User user) {
        return new Cookie("userId", Base64.getEncoder().encodeToString(user.getLogin().getBytes()));
    }

    private Map<String, String> checkUser(User user) {
        Map<String, String> messages = new HashMap<>();
        if (user == null || user.getLogin() == null || user.getPassword() == null) {
            messages.put("userObjectException", "Cannot read required arguments");
        } else {
            String password = this.users.get(user.getLogin());
            if (password == null || !password.equals(user.getPassword())) {
                messages.put("wrongPasswordException", "Login and password don't match");
            }
        }
        return messages;
    }
}
