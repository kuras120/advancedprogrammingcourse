package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "controller.DashboardServlet", urlPatterns = {"/dashboard", "/dashboard/*"})
public class DashboardServlet extends HttpServlet {

    private Gson gson;

    @Override
    public void init() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        List<Book> books = ((BookList)request.getServletContext().getAttribute("books")).getBooks();
        gson.toJson(new Response(books, 200), response.getWriter());
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Book book;
        response.setContentType("application/json;charset=UTF-8");
        User user = (User)request.getSession().getAttribute("user");
        if (!"admin".equals(user.getLogin())) {
            response.setStatus(403);
            return;
        }
        BookList bookList = (BookList)request.getServletContext().getAttribute("books");
        try {
            book = gson.fromJson(IOUtils.toString(request.getReader()), Book.class);
        } catch (Exception ex) {
            ExceptionResponse exResponse = new ExceptionResponse();
            exResponse.setData(Map.of("jsonParseException", ex.getLocalizedMessage()));
            exResponse.setStatus(500);
            response.setStatus(500);
            gson.toJson(exResponse, response.getWriter());
            return;
        }
        Map<String, String> messages = checkBook(book);
        if (messages.isEmpty()) {
            bookList.setIdCounter(bookList.getIdCounter() + 1);
            book.setId(bookList.getIdCounter());
            bookList.getBooks().add(book);
            request.getServletContext().setAttribute("books", bookList);
            response.setStatus(201);
            gson.toJson(new Response(book, 201), response.getWriter());
        } else {
            response.setStatus(400);
            gson.toJson(new Response(messages, 400), response.getWriter());
        }
    }

    @Override
    public void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        User user = (User)request.getSession().getAttribute("user");
        if (!"admin".equals(user.getLogin())) {
            response.setStatus(403);
            return;
        }
        long id;
        try {
            System.out.println(request.getPathInfo().substring(1));
            id = Long.decode(request.getPathInfo().substring(1));
        } catch (Exception ex) {
            ExceptionResponse exResponse = new ExceptionResponse();
            exResponse.setData(Map.of("numberFormatException", ex.getLocalizedMessage()));
            exResponse.setStatus(500);
            response.setStatus(500);
            gson.toJson(exResponse, response.getWriter());
            return;
        }
        BookList bookList = (BookList)request.getServletContext().getAttribute("books");
        Book book = bookList.getBooks().stream().filter(x -> x.getId() == id).findFirst().orElse(null);
        if (book != null) bookList.getBooks().remove(book);
        response.setStatus(200);
        gson.toJson(new Response(book, 200), response.getWriter());
    }

    private Map<String, String> checkBook(Book book) {
        Map<String, String> messages = new HashMap<>();
        if (book.getTitle() == null || book.getTitle().isEmpty())
            messages.put("Title", "Cannot be empty");
        if (book.getAuthor() == null || book.getAuthor().isEmpty())
            messages.put("Author", "Cannot be empty");
        if (book.getYear() == null || book.getYear().isEmpty())
            messages.put("Year", "Cannot be empty");
        if (book.getYear() != null && !StringUtils.isNumeric(book.getYear()))
            messages.put("Year", "Cannot parse to number");
        return messages;
    }
}
