Główny katalog z sourcem (src/main/*)
1. Java zawiera:
	- controller zawiera servlety
	- filter zawiera filtry
	- listener zawiera listenery
	- model zawiera modele
2. Resources zawiera metadane (obecnie nic ważnego)
3. Webapp zawiera stronę JSP, w razie konieczności wyświetlenia customowej strony błędu (domyślnie jest zwracany json z informacją o błędzie)

Reszta katalogów jest nieistotna - testy do aplikacji nie zostały utworzone.
